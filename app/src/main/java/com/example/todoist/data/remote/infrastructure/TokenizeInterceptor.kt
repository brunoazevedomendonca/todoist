package com.example.todoist.data.remote.infrastructure

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

class TokenizeInterceptor @Inject constructor() : Interceptor {

    // TODO - Configurar token:
    //  acessar https://developer.todoist.com/appconsole.html
    //  criar conta
    //  criar app
    //  criar test token
    companion object {
        private const val API_TEST_TOKEN = ""
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest: Request = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer $API_TEST_TOKEN")
            .build()
        return chain.proceed(newRequest)
    }
}